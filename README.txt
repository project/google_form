Allows Google Forms associated with Drupal, being loaded and submitted via
Drupal in order to avoid horrible iFrames!

Requires the simple_html_dom library installed in
site/all/libraries/simplehtmldom directory,

The library itself can be found at:
http://sourceforge.net/projects/simplehtmldom/

Requires the libraries api module available via drush (libraries) or
http://drupal.org/project/libraries

INSTALL:

1. Download Simple HTML Dom library and place the downloaded PHP file in
   /sites/all/libraries/simplehtmldom (you will need to create this folder)
   depending on your webhost configuration you may need to make sure this file
   is executable
   
2. Install the Libraries module as per the instructions here:
   http://drupal.org/project/libraries
   
3. Upload the Google Form module to your modules folder

4. Activate it!

USAGE:

NOTE: I assume that you already have created a google form already and have its
form URL.

user needs to have the following permissions:

google_form create		
google_form delete		
google_form read

within the google forms section under Site Building, you can now associate
a google form with your site, to do this:

Click "Associate new Google Form"
Enter a Title (this is purely descriptive and can be used for your block etc)
Enter the form key, this is found by taking your form url and taking all the
text after "formkey="
Click Save

to view your form on your site, go into your blocks section and display the form
in the area of your site you wish to, using the relevant page filters as
appropriate for your site.

Navigate to the page and check out your google form!

CREDITS:

Original Module Work: Tom Gillett - http://www.tomgillett.co.uk
Further Coding and Readme: Anthony Somerset - http://somersettechsolutions.co.uk