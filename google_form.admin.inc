<?php
/**
 * @file
 * Priovides admin interface for google_form in the admin section of Drupal
 *
 */

 /**
 * Lists the google forms associated with the drupal site.
 */
function google_form_list() {
  $output = t('Use this list to manage associated google forms.');
  $results = _google_form_find('all');

  if (!empty($results)) {
    $header = array(t('Name'), t('Form Key'), t('Actions'));

    $rows = array();
    foreach ($results as $result) {
      $rows[] = array($result['title'], $result['form_key'], l(t('Delete'), 'admin/build/google_forms/delete/'. $result['gfid']));
    }

    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('To associate your first google form, please use the above link.');
  }

  return $output;
}

/**
 * Returns Associate google form fields for adding a google form to your site
 */
function google_form_add() {
  $output = t('Use this form to associate a new google form.');
  $output .= drupal_get_form('google_form_settingsform');
  return $output;
}

/**
 * Deletes associated google form with confirmation question
 */
function google_form_delete($form_state, $id) {
  $result = _google_form_find($id);
  if (empty($result)) {
    drupal_set_message(t('The specified google form association was not found'), 'error');
    drupal_goto('admin/build/google_forms');
  }

  $form = array();
  $form['gfid'] = array('#type' => 'value', '#value' => $result['gfid']);
  return confirm_form(
    $form,
    t('Are you sure you want to delete the `%title` google form association?', array('%title' => $result['title'])),
    'admin/build/google_forms',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Removes associated google form from database
 */
function google_form_delete_submit($form, &$form_state) {
  $gfid = $form_state['values']['gfid'];
  $result = db_query('DELETE FROM {google_form} WHERE gfid = %d', $gfid);

  if ($result) {
    drupal_set_message(t('Google form association was deleted.'));
  }

  $form_state['redirect'] = 'admin/build/google_forms';
}

/**
 * Form function for entering the formkey details from google
 */
function google_form_settingsform() {
  $vocabularies = taxonomy_get_vocabularies();
  if (!empty($vocabularies)) {
    foreach ($vocabularies as $key => $vocabulary) {
      $output[$vocabulary->vid] = $vocabulary->name .' (Associated with: ' . implode(',', $vocabulary->nodes) . ')';
    }

    $vocabularies = $output;
  }

  $form = array(
    'essential' => array(
      '#title' => t('Required Settings'),
      '#type' => 'fieldset',
      '#description' => t('These settings are required when associating a google form.'),

      'title' => array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#description' => t('Please create a name for this google form.'),
      ),

      'form_key' => array(
        '#type' => 'textfield',
        '#title' => t('Form Key'),
        '#description' => t('Please enter the form key for the google form you wish to associate. This can be found in the URL of the google form (key=...).'),
      ),
    ),

    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );

  return $form;
}

/**
 * Validates input to google_form_settingsform().
 */
function google_form_settingsform_validate($form, &$form_state) {
  if (!preg_match('/^[a-zA-Z0-9\_\s]{1,100}$/', $form_state['values']['title'])) {
    form_set_error('title', t('Title must be specified, less than 100 alphanumeric characters, and should contain no symbols (other than _).'));
  }
  else {
    $result = db_fetch_array(db_query("SELECT COUNT(gfid) FROM {google_form} WHERE title = '%s'", $form_state['values']['title']));
    if (current($result) > 0) {
      form_set_error('title', t('It appears the title you specified is already in use. Please specify a unique title.'));
    }
  }

  if (!preg_match('/^[a-zA-Z0-9]{1,100}$/', $form_state['values']['form_key'])) {
    form_set_error('form_key', t('Key must be specified, less than 100 alphanumeric characters. It must not contain any spaces or symbols.'));
  }
}

/**
 * Performs database insert on form creation via google_form_settingsform().
 */
function google_form_settingsform_submit($form, &$form_state) {
  $record = new stdClass();
  $record->title = $form_state['values']['title'];
  $record->form_key = $form_state['values']['form_key'];

  if (drupal_write_record('google_form', $record)) {
    drupal_set_message(t('Google form association was created.'));
    drupal_goto('admin/build/google_forms');
  }
}
